export const SWENSEN_MODEL = {
    Description: ` El "Modelo de Swensen" podría referirse a varias cosas en diferentes contextos, pero uno de los conceptos más conocidos relacionados con Swensen es el "Modelo de Cartera de Swensen". 
        David F. Swensen fue el director de inversiones de la Universidad de Yale y es conocido por su enfoque de inversión en carteras de activos. 
        Su enfoque se centra en la gestión de activos de manera diversificada y a largo plazo. 
    `,
    Diversification: `Swensen aboga por una diversificación amplia de activos en la cartera de inversiones. 
        Esto incluye acciones, bonos, inversiones alternativas como bienes raíces y activos privados, así como una variedad de clases de activos internacionales.
    `,
    Actives: `
        Sugiere que los inversores deben determinar una asignación de activos estratégica basada en sus objetivos financieros a largo plazo y su tolerancia al riesgo. 
        Esta asignación estratégica debe equilibrar diferentes clases de activos para lograr el crecimiento y la estabilidad.
    `,
    Alternative_Inversions: `
        Swensen aboga por una mayor inversión en activos no tradicionales como bienes raíces, capital privado y otros activos alternativos, 
        que pueden proporcionar diversificación adicional y rendimientos atractivos.
    `,
    Long_Term: `
        Su enfoque se centra en mantener una cartera durante un período prolongado, a menudo décadas, en lugar de intentar cronometrar el mercado. 
        Esto implica mantener una asignación estratégica de activos y realizar ajustes periódicos en lugar de reaccionar a las fluctuaciones del mercado a corto plazo.
    `,
    Minimized_Cost: `
        Swensen también presta atención a minimizar los costos de inversión, como las comisiones y los gastos de gestión, lo que puede mejorar el rendimiento a largo plazo.
    `
}

export const PROJECT_GOAL = `
    Dicho todo lo anterior, queda mas que claro que el objetivo de este proyecto es el de monitorear de manera mas amigable las inversiones a largo plazo para llegar la libertad financiera.
    Que si bien, no es fácil, una manera de empezar hacerlo es mediante la constancia y enfoques pequeños mes con mes.
`;