export enum EPaths {
    HOME = '/',
    PORTFOLIO = '/portfolio',
    ETF_CHART = '/etf-chart',
    EXPENSES = '/expenses',
    EXPENSES_HISTORY = '/expenses/history',
    EXPENSES_CREATE = '/expenses/create'
}

export enum EApiPaths {
    BASE_PATH = '/api/finances/v1',   
}