import { Box } from '@mui/material'
import { FC, ReactNode } from 'react'

type IBody = {
    children?: ReactNode
}

export const Body:FC<IBody> = ({children}) => {
    return (
        <Box
            sx={{
                backgroundColor: (theme) =>
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[900],
                flexGrow: 1,
                height: '100vh',
                overflow: 'auto',
            }}
        >
            <Box sx={{ position: 'relative', top: '64px' }}>
                {children}
            </Box>
        </Box>
    )
}