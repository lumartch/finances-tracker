import { FC, ReactNode, useContext, useState } from 'react';
import { Box, Button, Grid, IconButton, Toolbar, Typography, useTheme } from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import { ColorModeContext } from '../theme/ColorModeContext';

import { SkelletonMenu } from './SkelletonMenu';
import { AppBar } from './AppBar';
import { Drawer } from './Drawer';
import { Body } from './Body';

type ISkelleton = {
    children?: ReactNode;
}

export const Skelleton: FC<ISkelleton> = ({ children }) => {
    const theme = useTheme();
    const colorMode = useContext(ColorModeContext);
    const [open, setOpen] = useState<boolean>(false);
    const toggleDrawer = () => {
        setOpen(!open);
    };
    return (
        <Box sx={{ display: 'flex', color: 'text.primary' }}>
            <AppBar open={open} position='fixed'>
                <Toolbar>
                    <IconButton onClick={toggleDrawer} edge='start' sx={{ padding: '16px' }}>
                        { !open ? <MenuIcon /> : <ChevronLeftIcon /> }
                    </IconButton>
                    <Grid item container xs={12}>
                        <Grid item container xs={4}>
                            <Typography variant='h6' sx={{ padding: '16px' }}>
                                Finances Tracker
                            </Typography>
                        </Grid>
                        <Grid item container xs={8} justifyContent='end'>
                            <Button endIcon={theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />} 
                                onClick={colorMode.toggleColorMode} sx={{ fontSize: '1.1rem', color: 'text.primary' }}>
                                {theme.palette.mode} mode
                            </Button>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Drawer variant='permanent' open={open}>
                <SkelletonMenu />
            </Drawer>
            <Body>
                {children}
            </Body>
        </Box>
    );
}