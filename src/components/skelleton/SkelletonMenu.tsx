import Image from 'next/image'
import { Divider, List, ListItemButton, ListItemIcon, ListItemText, ListSubheader } from '@mui/material';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import SsidChartIcon from '@mui/icons-material/SsidChart';
import PaymentIcon from '@mui/icons-material/Payment';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import HomeIcon from '@mui/icons-material/Home';

import { EPaths, MAIN_DOMAIN } from '@/const';
import Link from 'next/link';

export const SkelletonMenu = () => {
    const { HOME, PORTFOLIO, ETF_CHART, EXPENSES_CREATE, EXPENSES_HISTORY } = EPaths;
    return (
        <>
            <List>
                <ListSubheader sx={{ lineHeight: '55px' }}>
                    Herramientas
                </ListSubheader>
                <Divider />
                <Link href={HOME} style={{ textDecoration: 'none' }} >
                    <ListItemButton sx={{color: 'text.primary'}}>
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary='Home' />
                    </ListItemButton>
                </Link>
                <Link href={PORTFOLIO} style={{ textDecoration: 'none' }}>
                    <ListItemButton sx={{color: 'text.primary'}}>
                        <ListItemIcon>
                            <AccountBalanceWalletIcon />
                        </ListItemIcon>
                        <ListItemText primary='Portafolio' />
                    </ListItemButton>
                </Link>
                <Link href={ETF_CHART} style={{ textDecoration: 'none' }}>
                    <ListItemButton sx={{color: 'text.primary'}}>
                        <ListItemIcon>
                            <SsidChartIcon />
                        </ListItemIcon>
                        <ListItemText primary='ETFs' />
                    </ListItemButton>
                </Link>
                <Link href={EXPENSES_CREATE} style={{ textDecoration: 'none' }}>
                    <ListItemButton sx={{color: 'text.primary'}}>
                        <ListItemIcon>
                            <PaymentIcon />
                        </ListItemIcon>
                        <ListItemText primary='Agregar gastos' />
                    </ListItemButton>
                </Link>
                <Link href={EXPENSES_HISTORY} style={{ textDecoration: 'none' }}>
                    <ListItemButton sx={{color: 'text.primary'}}>
                        <ListItemIcon>
                            <QueryStatsIcon />
                        </ListItemIcon>
                        <ListItemText primary='Historial de gastos' />
                    </ListItemButton>
                </Link>
            </List>
            <List sx={{ position: 'absolute', bottom: 0 }}>
                <Divider />
                <ListItemButton href={ MAIN_DOMAIN! } target='_blank'>
                    <ListItemIcon>
                        <Image src='/favicon.ico' alt='me' width='28' height='28' />
                    </ListItemIcon>
                    <ListItemText primary='Ir a LumartCh.dev' />
                </ListItemButton>
                <Divider />
            </List>
        </>
    );
}