import { EPaths, PROJECT_GOAL, SWENSEN_MODEL } from '@/const';
import { Button, Grid, Typography } from '@mui/material';
import Image from 'next/image';
import Link from 'next/link';

const Home = () => {
  const { Actives, Alternative_Inversions, Description, Diversification, Long_Term, Minimized_Cost} = SWENSEN_MODEL;
  return (
    <Grid item container p={8} xs={12}>
        <Grid item xs={12}>
            <Typography variant='h2'>Inicio</Typography>
        </Grid>
        <Grid container item xs={12} p={4} gap={4}>
            <Grid item xs={12}>
              <Typography variant='h5'>¿Qué es el modelo de Swensen?</Typography>
            </Grid>
            <Grid item xs={6} >
                <Typography>{Description}</Typography>
                <ul>
                  <li><Typography>Diversificación: {Diversification}</Typography></li>
                  <li><Typography>Asignación de activos estratégica: {Actives}</Typography></li>
                  <li><Typography>Inversiones alternativas: {Alternative_Inversions}</Typography></li>
                  <li><Typography>Enfoque a largo plazo: {Long_Term}</Typography></li>
                  <li><Typography>Minimización de costos: {Minimized_Cost}</Typography></li>
                </ul>
            </Grid>
            <Grid item xs={5}>
              <Image src='/SwensenModel.png' width={700} height={450} alt='Swensen Model' />
            </Grid>
        </Grid>
        <Grid item container xs={12} p={4} gap={2}>
          <Typography>{PROJECT_GOAL}</Typography>
          <Link href={EPaths.PORTFOLIO} style={{ textDecoration: 'none' }} >
            <Button variant='outlined'>Ver portafolio</Button>
          </Link>
        </Grid>
    </Grid>
  )
}

export default Home;