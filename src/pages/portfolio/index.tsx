import { Grid, Typography } from '@mui/material';
import { PORTFOLIO_TITLE } from '@/const';

const Portfolio = () => {
    return (
        <Grid item container p={8} xs={12}>
            <Grid item xs={12}>
                <Typography variant='h3'>{PORTFOLIO_TITLE}</Typography>
            </Grid>
        </Grid>
    );
}

export default Portfolio;