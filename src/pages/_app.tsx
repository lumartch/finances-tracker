import Head from 'next/head';
import type { AppProps } from 'next/app'
import { Skelleton, Theme } from '@/components';

import '@/styles/globals.css';

export default function App({ Component, pageProps }: AppProps) {
  return (
        <Theme>
          <Head>
              <title>Finances Tracker</title>
          </Head>
          <Skelleton>
            <Component {...pageProps} />
          </Skelleton>
        </Theme>
  );
}