import { Grid, Typography } from '@mui/material';
import { ETF_TITLE } from '@/const';

const ETFChart = () => {
    return (
        <Grid item container p={8} xs={12}>
            <Grid item xs={12}>
                <Typography variant='h3'>{ETF_TITLE}</Typography>
            </Grid>
        </Grid>
    );
}

export default ETFChart;