import { Grid, Typography } from '@mui/material';
import { CREATE_EXPENSES_TITLE } from '@/const';

const CreateExpenses = () => {
    return (
        <Grid item container p={8} xs={12}>
            <Grid item xs={12}>
                <Typography variant='h3'>{CREATE_EXPENSES_TITLE}</Typography>
            </Grid>
        </Grid>
    );
}

export default CreateExpenses;