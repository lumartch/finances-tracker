import { Grid, Typography } from '@mui/material';
import { HISTORY_EXPENSES_TITLE } from '@/const';

const HistoryExpenses = () => {
    return (
        <Grid item container p={8} xs={12}>
            <Grid item xs={12}>
                <Typography variant='h3'>{HISTORY_EXPENSES_TITLE}</Typography>
            </Grid>
        </Grid>
    );
}

export default HistoryExpenses;