import axios, { Axios } from 'axios';

import { EApiPaths } from '@/const';

export const ApiHandler = (() => {
    let instance: any;
    const createInstance = () => {
        const axiosInstance: Axios = axios.create({ baseURL: `${EApiPaths.BASE_PATH}`, timeout: 10000 });
        return { axiosInstance };
    }

    return {
        getInstance: () => {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        },
    };
})();